import javax.swing.JOptionPane;

public class Month 
{
	public enum Mon
	{
		Month,January, February, March, April, May, June, July, August, September, November, December,October
	}
	public static void main (String[] args)
	{
		Mon[] choices = 
        	{ 
				Mon.Month,Mon.January,Mon.February, Mon.March, Mon.April, Mon.May, Mon.June, Mon.July, Mon.August, Mon.September,Mon.October, Mon.November, Mon.December
            };
		Mon select;
		do
		{
		select = (Mon)JOptionPane.showInputDialog(null,"Select Month","Month",JOptionPane.INFORMATION_MESSAGE,null,choices,choices[0]);
		
			
			switch (select)
			{
			case December:
			case January:
			case February:
				JOptionPane.showMessageDialog(null, "Do you want to build a snow man?");
				break;
			case March:
			case April:
			case May:
				JOptionPane.showMessageDialog(null, "Happy Spring Days!");
				break;
			case June:
			case July:
			case August:
				JOptionPane.showMessageDialog(null, "It's a summer time.");
				break;
			case September:
			case October:
			case November:
				JOptionPane.showMessageDialog(null, "Welcome to the foliage season");
				break;
			default:
				break;
			}
		}while (!select.equals(JOptionPane.CANCEL_OPTION));
	}
}
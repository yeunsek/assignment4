
public class CarClass 
{
		private String color;
		private double horsePower;
		private double engineSize;
		
		public CarClass()
		{
			this.color = "";
			this.horsePower = 0;
			this.engineSize = 0;
		}
		public CarClass(String color, double horsePower, double engineSize)
		{
			this.color = color;
			this.horsePower = horsePower;
			this.engineSize = engineSize;
		}
		public String getColor()
		{
			return this.color;
		}
		public void setColor(String color)
		{
			this.color = color;
		}
		public double getHorsePower()
		{
			return this.horsePower;
		}
		public void setHorsePower(double horsePower)
		{
			this.horsePower = horsePower;
		}
		public double getEngineSize()
		{
			return this.engineSize;
		}
		public void setEngineSize(double engineSize)
		{
			this.engineSize = engineSize;
		}
		public String toString()
		{
			return "[Color: " + this.color + " Horse Power: "+this.horsePower+" Engine Size: "+this.engineSize +" Liters ]";
		}
}
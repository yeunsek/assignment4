public class Loops 
{

	public static void main(String[] args) 
	{
		int odd = 1;
		int even = 2;
		int c = 0;
		int j = 1;
		int k = 0;
	
		
		System.out.println("Odd numbers: ");
		for(int i=0; i<100; i++)
		{
			System.out.print(odd+" ");
			odd = odd +2;
		}
		System.out.println();
		System.out.println ("Even numbers: ");
		do
		{
			System.out.print(even+" ");
			even = even +2;
			c++;
		} while (c<100);
		System.out.println();
		
		while (j<7)
		{
			while (k<5)
			{
				System.out.print(" ");
				k+=2;
			}
			k=0;
			while (k<j)
			{
				System.out.print("*");
				k++;
			}
			System.out.println();
			j += 2;
		}
		j=3; k=0;
		while (j>0)
		{
			while (k<3)
			{
				System.out.print(" ");
				k+=2;
			}
			k=0;
			while (k<j)
			{
				System.out.print("*");
				k++;
			}
			System.out.println();
			j-=2; k=-2;
		}
	}
}

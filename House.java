/*Author: Eun Suk Lee
 *Email: el24336@email.vccs.edu
 *Date: 4/8/2015
 *This program will show simulation of the room
 */

public class House 
{
	private String color;//color of wall
	private String floor;//floor type
	private String windows;//number of windows
	
	public House()//initiate the values
	{
		this.color = "";//initiate it to space
		this.floor = "";//initiate it to space
		this.windows = "";//initiate it to space
	}
	public House(String color, String floor, String windows)//input format
	{
		this.color = color;//set color to color
		this.floor = floor;//set floor to floor
		this.windows = windows;//set windows to windows
	}
	public String getColor()//get color from the user
	{
		return this.color;
	}
	public void setColor(String color)//set color from the input
	{
		this.color = color;
	}
	public String getFloor()//get floor from the user
	{
		return this.floor;
	}
	public void setFloor(String floor)//set floor from the input
	{
		this.floor = floor;
	}
	public String getWindows()//get windows from the user
	{
		return this.windows;
	}
	public void setWindows(String windows)//set windows from the input
	{
		this.windows = windows;
	}
	public String toString()//output format
	{
		return "Wall color: " + this.color + "\nFloor: "+this.floor+"\nWindows: "+this.windows;
	}
}